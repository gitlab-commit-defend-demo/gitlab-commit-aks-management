# gitlab-commit-aks-management

This repo contains everything to manage our Kubernetes cluster

## Cilium with AKS

Following manuall step is needed to configure Cilium with AKS:
``` bash
kubectl apply -f https://gitlab.com/gitlab-commit-defend-demo/gitlab-commit-aks-management/-/raw/master/yaml/cilium-cni.yaml?inline=false
```
